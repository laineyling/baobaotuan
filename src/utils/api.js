import service from './request'

// 一个请求 封装成一个函数
// 将整个项目中的所有请求，都集中管理到这个api.js文件中
export function shop_list( params = {} ){
    return service.get('/shop/list',{params});
}

export function tuan_list( params = {}){
    return service.get('/tuan/list',{params: params});// params: params可简写为params
}

// export function user_login( params = {} ){
//     return service.post('/user/login',params);
// }

// 命名抛出
// export {
//     shop_list,
//     tuan_list,
//     user_login,
// }