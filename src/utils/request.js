// axios二次封装
// 导入axios
import axios from 'axios'

// 实例化axios对象
var service = axios.create({
    timeout:10*1000, //设置请求超时事件
    baseURL:'/api',//请求地址的基准路径，假设在代码中发请求 /user/list ，实际发出的请求时 /api/user/list
})
// 设置请求拦截器
service.interceptors.request.use(
(config)=>{//请求发出前会立即自动执行的回调函数

    // 统一设置请求头
    config.headers['token'] = 'vsdbsdbds';
    config.headers['x-token'] = '524jtyky';

    return config;

},(error)=>{//请求发出前，发生错误的回调函数
    return Promise.reject(error);
})
// 设置响应拦截器
service.interceptors.response.use(
(res)=>{//设置从服务器返回前会立即自动执行的回调函数
    return res;
},(error)=>{//设置从服务器返回前,发生错误的回调函数
    // error是响应错误对象
    if(error.response.status == 401){
        console.log('身份认证过期，请重新登录！');
    }else if(error.response.status == 404){
        console.log('访问路径有误');

    }else if(error.response.status == 500){
        console.log('服务器内部错误！请联系管理员处理!');
    }
})

// 抛出axios
export default service;