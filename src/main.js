import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 导入组件
import Swiper from './components/Swiper.vue'
import FlashSaleList from './components/FlashSaleList.vue'
import Search from './components/Search.vue'
import Swiper1 from './components/Swiper1.vue'
import List1 from './components/List1.vue'
// 注册组件(全局组件，可以在所有.Vue文件中使用)
Vue.component('Swiper',Swiper);
Vue.component('FlashSaleList',FlashSaleList);
Vue.component('Search',Search);
Vue.component('Swiper1',Swiper1);
Vue.component('List1',List1);

//实例化Vue对象
new Vue({
  render: h => h(App),
}).$mount('#app')
