const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
    proxy:{
      '/api':{
        target:'http://waimai.yantianfeng.com/',
        changeOrigin:true,
        pathRewrite:{}
      }
    }
  }
})
